# NGramJ Language Extraction service

![WebLab-Core 1.2.8](https://img.shields.io/badge/WebLab--Core-1.2.8-brightgreen.svg)
![LGPL V2.1](https://img.shields.io/badge/license-LGPL%20V2.1-brightgreen.svg)

This project is a Maven based Java project of a Web Service.
This WebLab WebService is wrapping NGramJ API (updated by LatinoJ project) to identify language in Text MediaUnit.
This algorithm return for each input text a score associated to every language profile previously learned (.ngp files profided i configuration). The score is a double between 0 and 1. 1 meaning that this text is written in this language for sure. 0 on the opposite means that this text is not written in this language. The sum of score equals 1.

The wrapper annotate every Text section of a Document in input (or the Text if the input is a Text). It fails if the input is something else. On each Text it uses CNGram to determine which language profile are the best candidate to be annotated (using Dublin Core language). It can also annotates the main Document.


# Build status
## Develop

[![build status](https://gitlab.ow2.org/weblab/services/ngramj-language-extraction/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/services/ngramj-language-extraction/commits/develop)



## Master

[![build status](https://gitlab.ow2.org/weblab/services/ngramj-language-extraction/badges/master/build.svg)](https://gitlab.ow2.org/weblab/services/ngramj-language-extraction/commits/master)


# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 

# How to use it

## Configuration

To configure the instance to be deployed all you need is to customise the applicationContext.xml file provided in the war. (You could also use WebLab configuration overriding mechanism that let you only update the file in conf/services folder at the root of the bundle).
This Spring/CXF configuration file will let you configure the constructor arguments to be provided to the instance.
The main information that could be customised is the path to the default profiles (ngp files). By default it relies on the models provided inside the jar of cngram, but you can set an external folder and provide custom models inside.


It can be configured using the CXF/Spring bean loading IoC pattern through constructorArgs where you can provide an instance of LanguageExtractionConfiguration. This object is a bean with 11 fields.
* maxNbValues: It's a positive integer value. The list of annotated language on a given Text could not be greater that this value.
* addTopLevelAnnot: It's a boolean value. It defines whether or not to annotate the whole document with the language extracted from the concatenation of every Text content.
* addMediaUnitLevelAnnot: It's a boolean value. It defines whether or not to annotate the each Text section with the language guessed.
* profilesFolderPath: It's a String that represents a folder path; This folder contains .ngp files that will be loaded instead of default CNGram 28 languages.
* isProducedByObject: It's a String value that should be a valid URI. It defines the URI to be used as object of every isProducedBy statements on annotations created by the service.
* unknownLanguageCode: It's the String value that will be annotated when no language can be clearly identified. When null, nothing is annotated.
* minSingleValue: It's a double value between 0 and 1. If the best language score is greater than this value, it will be the only one annotated on a given Text.
* minMultipleValue: It's a double value between 0 and 1. Every language score that are greater than this value, will be annotated on a given Text.
* minContentLength: It's an Integer. The minimum length of the String to be analysed. Otherwise it is simply ignored.
* pathToBlackList: The path to the black list file (containing string to be ignored from the ngrams)
* mode: It's an integer value. This is used inside Cngram internal algorithm and has impact on the score computation.


Those 11 properties are optional. Default values are:

* maxNbValues: 1
* addTopLevelAnnot: false
* addMediaUnitLevelAnnot: true
* profilesFolderPath: in this case, we use the default constructor for CNGram profile that will use default profile given in their jar file. These 28 profiles are named using ISO 639-1 two letters language code; it means that the DC:language annotation resulting will be in this format. If you want to use another format, you have use a custom profiles folder (containing .ngp files).
* isProducedByObject: null in this case, no isProducedBy annotation will be created.
* unknownLanguageCode: und is used, since it is the code for undetermined in ISO-639-x.
* minSingleValue: 0.75
* minMultipleValue: 0.15
* minContentLength: 5, i.e. almost every text is processed.
* pathToBlackList: null, i.e. nothing is ignored.
* mode: 1

## UsageContext effects

UsageContext is not used in this service.

## Examples of SOAP Input/Output
### Analyser:process
#### Input
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:anal="http://weblab.ow2.org/core/1.2/services/analyser">
   <soapenv:Header/>
   <soapenv:Body>
      <anal:processArgs>
         <resource xsi:type="model:Document" uri="weblab://SmallEnglishTest/1" xmlns:model="http://weblab.ow2.org/core/1.2/model#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <mediaUnit xsi:type="model:Text" uri="weblab://SmallEnglishTest/1#0">
               <content>WebLab: An integration infrastructure to ease the development of multimedia processing applications
Patrick GIROUX, Stephan BRUNESSAUX, Sylvie BRUNESSAUX, Jérémie DOUCY, Gérard DUPONT, Bruno GRILHERES, Yann MOMBRUN, Arnaud SAVAL
Information Processing Control and Cognition (IPCC)
EADS Defence and Security Systems
Parc d'Affaire des Portes
27106 Val de Reuil

http://weblab-project.org

ipcc@weblab-project.org

{patrick.giroux, stephan.brunessaux, sylvie.brunessaux, jeremie.doucy, gerard.dupont, bruno.grilheres, yann.mombrun, arnaud.saval}@eads.com

Abstract:
In this paper, we introduce the EADS' WebLab platform (http://weblab-project.org) that aims at providing an integration infrastructure for multimedia information processing components. In the following, we explain the motivations that have led to the realisation of this project within EADS and the requirements that have led our choices. After a quick review of existing information processing platforms, we present the chosen service oriented architecture, and the three layers of the WebLab project (infrastructure, services and applications).
Then, we detail the chosen exchange model and normalised services interfaces that enable semantic interoperability between information processing components. We present the technical choices made to guarantee technical interoperability between the components by the use of an Enterprise Service Bus (ESB).
Moreover, we present the orchestration and portal mechanisms that we have added to the WebLab to enable architects to quickly build multimedia processing applications. In the following, we illustrate the integration process by describing three applications that have been developed on top of this architecture on three R&amp;D projects (Vitalas, WebContent and eWok-Hub). Finally, we propose some perspectives such as the realisation of an information processing services directory, or a toolkit following MDA (Model Driven Architecture) approach to ease the integration process.

Keywords:
Integration infrastructure, Service Oriented Architecture, Semantics, Multimedia Information Processing Platform.</content>
            </mediaUnit>
         </resource>
      </anal:processArgs>
   </soapenv:Body>
</soapenv:Envelope>
```

#### Output
```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns6:processReturn xmlns:wl="http://weblab.ow2.org/core/1.2/model#" xmlns:ns6="http://weblab.ow2.org/core/1.2/services/analyser">
         <resource xsi:type="wl:Document" uri="weblab://SmallEnglishTest/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <annotation uri="weblab://SmallEnglishTest/1#a2">
               <data>
                  <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">
                     <rdf:Description rdf:about="weblab://SmallEnglishTest/1">
                        <dc:language>en</dc:language>
                     </rdf:Description>
                     <rdf:Description rdf:about="weblab://SmallEnglishTest/1#a2" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:wp="http://weblab.ow2.org/core/1.2/ontology/processing#">
                        <dcterms:created>2012-03-06T16:21:11+0100</dcterms:created>
                        <wp:isProducedBy rdf:resource="http://weblab.ow2.org/services#LanguageExtraction"/>
                     </rdf:Description>
                  </rdf:RDF>
               </data>
            </annotation>
            <mediaUnit xsi:type="wl:Text" uri="weblab://SmallEnglishTest/1#0">
               <annotation uri="weblab://SmallEnglishTest/1#0-a0">
                  <data>
                     <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">
                        <rdf:Description rdf:about="weblab://SmallEnglishTest/1#0">
                           <dc:language>en</dc:language>
                        </rdf:Description>
                        <rdf:Description rdf:about="weblab://SmallEnglishTest/1#0-a0" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:wp="http://weblab.ow2.org/core/1.2/ontology/processing#">
                           <dcterms:created>2012-03-06T16:21:11+0100</dcterms:created>
                           <wp:isProducedBy rdf:resource="http://weblab.ow2.org/services#LanguageExtraction"/>
                        </rdf:Description>
                     </rdf:RDF>
                  </data>
               </annotation>
               <content>WebLab: An integration infrastructure to ease the development of multimedia processing applications
Patrick GIROUX, Stephan BRUNESSAUX, Sylvie BRUNESSAUX, Jérémie DOUCY, Gérard DUPONT, Bruno GRILHERES, Yann MOMBRUN, Arnaud SAVAL
Information Processing Control and Cognition (IPCC)
EADS Defence and Security Systems
Parc d'Affaire des Portes
27106 Val de Reuil

http://weblab-project.org

ipcc@weblab-project.org

{patrick.giroux, stephan.brunessaux, sylvie.brunessaux, jeremie.doucy, gerard.dupont, bruno.grilheres, yann.mombrun, arnaud.saval}@eads.com

Abstract:
In this paper, we introduce the EADS' WebLab platform (http://weblab-project.org) that aims at providing an integration infrastructure for multimedia information processing components. In the following, we explain the motivations that have led to the realisation of this project within EADS and the requirements that have led our choices. After a quick review of existing information processing platforms, we present the chosen service oriented architecture, and the three layers of the WebLab project (infrastructure, services and applications).
Then, we detail the chosen exchange model and normalised services interfaces that enable semantic interoperability between information processing components. We present the technical choices made to guarantee technical interoperability between the components by the use of an Enterprise Service Bus (ESB).
Moreover, we present the orchestration and portal mechanisms that we have added to the WebLab to enable architects to quickly build multimedia processing applications. In the following, we illustrate the integration process by describing three applications that have been developed on top of this architecture on three R&amp;D projects (Vitalas, WebContent and eWok-Hub). Finally, we propose some perspectives such as the realisation of an information processing services directory, or a toolkit following MDA (Model Driven Architecture) approach to ease the integration process.

Keywords:
Integration infrastructure, Service Oriented Architecture, Semantics, Multimedia Information Processing Platform.</content>
            </mediaUnit>
         </resource>
      </ns6:processReturn>
   </soap:Body>
</soap:Envelope>
```
