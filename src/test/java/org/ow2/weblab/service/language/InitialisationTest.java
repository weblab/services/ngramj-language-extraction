/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.language;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.services.Analyser;
import org.springframework.context.support.FileSystemXmlApplicationContext;



/**
 * @author ymombrun
 */
public class InitialisationTest {


	@Test
	public void testWorkingConstructors() {

		final LanguageExtraction le = new LanguageExtraction();
		Assert.assertNotNull(le);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testTooSmallMaxNbValues() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(0);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testTooSmallMinSingleValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMinSingleValue(-1);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testTooBigMinSingleValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMinSingleValue(1.1);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testTooSmallMinMultipleValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMinMultipleValue(-1);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testTooBigMinMultipleValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMinMultipleValue(1.3);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testMisMatchSingleAndMultipleValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMinMultipleValue(0.9);
		lec.setMinSingleValue(0.1);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNonExistingBlackListPathValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setPathToBlackList("not/existing/path");
		lec.setMinSingleValue(0.1);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNotFileBlackListPathValue() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setPathToBlackList("src");
		lec.setMinSingleValue(0.1);
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testBadURI() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setIsProducedByObject("%");
		lec.validateConfig();
		Assert.assertNull(lec);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNonExistingFolder() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setProfilesFolderPath("target/folder/that/does/not/exists");
		Assert.assertNotNull(lec);
		Assert.assertNull(new LanguageExtraction(lec));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNonDirectory() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setProfilesFolderPath("pom.xml");
		Assert.assertNotNull(lec);
		Assert.assertNull(new LanguageExtraction(lec));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNonNGPDirectory() {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setProfilesFolderPath("src/main/resources");
		Assert.assertNotNull(lec);
		Assert.assertNull(new LanguageExtraction(lec));
	}


	@Test
	public void testDefaultConfig() {

		try (final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml")) {
			final Analyser analyser = context.getBean("analyser", Analyser.class);
			Assert.assertNotNull(analyser);
		}
	}


	@Test
	public void testDefaultNGramProfilePatched() throws IOException, ReflectiveOperationException {

		this.mutedInfo(new NGramProfilesPatched(1));
	}


	@Test()
	public void testCustomNGramProfilePatched() throws IOException, ReflectiveOperationException {

		this.mutedInfo(new NGramProfilesPatched(new File("src/test/resources/sampleProfiles"), 1));
	}


	private void mutedInfo(final NGramProfilesPatched profiles) {

		final PrintStream out = System.out;
		final PrintStream err = System.err;
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final PrintStream newOut = new PrintStream(baos);
		System.setOut(newOut);
		System.setErr(newOut);
		try {
			profiles.info();
		} catch (final IllegalArgumentException iae) {
			LogFactory.getLog(this.getClass()).fatal("An error occured printing info about models. There is a bug in original code when models seem to be close.", iae);
			// Swallow exception and do not fail for that
		} finally {
			try {
				LogFactory.getLog(this.getClass()).info(baos);
			} finally {
				System.setOut(out);
				System.setErr(err);
			}
		}
	}


}
