/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.language;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * Test the language extraction service in a MultiThreaded environment.
 *
 * @author EADS IPCC Team
 */
public class LaunchMTTest {


	private final static String[] LANGUAGES = { "en", "fr", "de", "ru", "pt", "es", "da", "el", "is" };


	private final static int NB_THREAD = 40;


	@Test
	public void testSearchInThread() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(false);
		lec.setAddMediaUnitLevelAnnot(true);
		lec.setIsProducedByObject("http://fakeURI/ngramJ/test");

		final LanguageExtraction le = new LanguageExtraction(lec);

		final List<Callable<Resource>> analysers = new ArrayList<>();
		final Document doc = new WebLabMarshaller().unmarshal(new File("src/test/resources/doc_multi_lingual.xml"), Document.class);

		LogFactory.getLog(this.getClass()).info("Begin thread creation.");
		for (int i = 1; i <= LaunchMTTest.NB_THREAD; i++) {
			final Document newDoc = WebLabResourceFactory.createResource("LanguageExtractionTest", "Thread-" + i, Document.class);

			for (final MediaUnit mu : doc.getMediaUnit()) {
				final Text t = WebLabResourceFactory.createAndLinkMediaUnit(newDoc, Text.class);
				t.setContent(((Text) mu).getContent());
			}

			final AnalyserMultiThread thread = new AnalyserMultiThread(le, newDoc, null);
			analysers.add(thread);
		}

		LogFactory.getLog(this.getClass()).info("Thread created.");

		final ExecutorService executor = Executors.newFixedThreadPool(LaunchMTTest.NB_THREAD);
		final List<Future<Resource>> results = executor.invokeAll(analysers, 5L, TimeUnit.MINUTES);

		int j = 0;
		for (final Future<Resource> future : results) {
			j++;
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			Assert.assertNotNull(future.get());

			final Document myDoc = ((Document) future.get());
			for (int i = 0; i < myDoc.getMediaUnit().size(); i++) {
				final Text t = (Text) myDoc.getMediaUnit().get(i);
				final DublinCoreAnnotator dcat = new DublinCoreAnnotator(t);
				Assert.assertTrue("Fail for i=" + i + " for Thread nb " + j + "\n Found: " + dcat.readLanguage().getValues() + "; Expected: " + LaunchMTTest.LANGUAGES[i] + "",
						dcat.readLanguage().getValues().contains(LaunchMTTest.LANGUAGES[i]));
			}
		}

		executor.shutdown();
		executor.shutdownNow();

		LogFactory.getLog(this.getClass()).info("Thread executed.");
	}

}
