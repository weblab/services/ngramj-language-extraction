/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.language;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * Test the language extraction service.
 *
 * @author EADS IPCC Team
 */
public class ServiceTest {


	private static final String POUET = "pouet";


	private static final String ENGLISH_SENTENCE = "This is a simple english sentence that should be detected as english (I hope so).";


	private final static String[] LANGUAGES = { "en", "fr", "de", "ru", "pt", "es", "da", "el", "is" };


	private final Log log = LogFactory.getLog(this.getClass());


	@Test
	public void testBlackListCompilation() throws IOException {

		final String pathToBlackList = "src/main/resources/emoticons.top100.withFreq";

		final Pattern bigPattern = new LanguageExtraction().blackListWithFreqCompilation(pathToBlackList);

		try (LineNumberReader rdr = new LineNumberReader(new FileReader(new File(pathToBlackList)))) {
			String line = null;
			while ((line = rdr.readLine()) != null) {
				final String[] values = line.split("\t");
				final String blackTerm = values[1];

				Assert.assertTrue(bigPattern.matcher(blackTerm).matches());
			}
		}

	}


	@Test
	public void testTextWithJunk() throws Exception {

		final String pathToBlackList = "src/main/resources/emoticons.top100.withFreq";

		final TreeSet<String> blackListedTerms = new TreeSet<>();

		try (LineNumberReader rdr = new LineNumberReader(new FileReader(new File(pathToBlackList)))) {
			String line = null;
			while ((line = rdr.readLine()) != null) {
				final String[] values = line.split("\t");
				blackListedTerms.add(values[1]);
			}
		}

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(false);
		lec.setAddMediaUnitLevelAnnot(true);
		lec.setMinSingleValue(0.2);
		lec.setMinMultipleValue(0.2);
		lec.setMinContentLength(5);
		lec.setPathToBlackList(pathToBlackList);
		final LanguageExtraction le = new LanguageExtraction(lec);

		final Pattern bigPattern = le.blackListWithFreqCompilation(pathToBlackList);

		final Text text = WebLabResourceFactory.createResource("test", "short-text", Text.class);
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(text);

		final String content = "This text is not too short and language should be detected";
		for (final String blackListedTerm : blackListedTerms) {
			for (int i = 0; i < content.length(); i++) {
				text.setContent(content);
				Matcher m = bigPattern.matcher(text.getContent());
				Assert.assertFalse("Regex should not match \nregex = " + bigPattern + "\n text = " + text.getContent(), m.find());

				text.setContent(content.substring(0, i) + blackListedTerm + content.substring(i));

				m = bigPattern.matcher(text.getContent());
				Assert.assertTrue("Regex does not match \nregex = " + bigPattern + "\n text = " + text.getContent(), m.find());

				pa.setResource(text);
				le.process(pa);
			}
		}
	}


	@Test
	public void testShortText() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(true);
		lec.setMinSingleValue(0.75);
		lec.setMinMultipleValue(0.15);
		lec.setMinContentLength(5);
		lec.setUnknownLanguageCode(ServiceTest.POUET);
		final LanguageExtraction le = new LanguageExtraction(lec);

		Text text = WebLabResourceFactory.createResource("test", "short-text", Text.class);
		text.setContent("12345"); // too short
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(text);

		new WebLabMarshaller().marshalResource(text, new File("target/testShortText.xml"));

		le.process(pa);
		DublinCoreAnnotator dca = new DublinCoreAnnotator(text);

		Assert.assertEquals(ServiceTest.POUET, dca.readLanguage().firstTypedValue());

		text = WebLabResourceFactory.createResource("test", "short-text", Text.class);
		text.setContent("This text is not too short and language should be detected");
		pa.setResource(text);
		le.process(pa);
		dca = new DublinCoreAnnotator(text);

		Assert.assertEquals("en", dca.readLanguage().firstTypedValue());
	}


	@Test
	public void testService1() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(false);
		lec.setAddMediaUnitLevelAnnot(true);
		lec.setIsProducedByObject("http://fakeURI/ngramJ/test");
		final LanguageExtraction le = new LanguageExtraction(lec);

		final Document doc = new WebLabMarshaller().unmarshal(new File("src/test/resources/doc_multi_lingual.xml"), Document.class);

		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(doc);

		le.process(pa);

		new WebLabMarshaller().marshalResource(doc, new File("target/doc_multi_lingual_with_annotation.xml"));

		for (int i = 0; i < doc.getMediaUnit().size(); i++) {
			final Text t = (Text) doc.getMediaUnit().get(i);
			final DublinCoreAnnotator dca = new DublinCoreAnnotator(t);
			this.log.debug("Expected: [" + ServiceTest.LANGUAGES[i] + "] vs Prediction: " + dca.readLanguage().getValues());

			Assert.assertTrue("Fail for i=" + i + " (Found: " + dca.readLanguage().getValues() + "; Expected: " + ServiceTest.LANGUAGES[i] + ")",
					dca.readLanguage().getValues().contains(ServiceTest.LANGUAGES[i]));
		}
	}


	@Test
	public void testService2() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(false);
		lec.setAddMediaUnitLevelAnnot(true);
		final LanguageExtraction le = new LanguageExtraction(lec);

		final Text t = WebLabResourceFactory.createResource("test", "test2", Text.class);
		t.setContent(ServiceTest.ENGLISH_SENTENCE);

		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(t);

		le.process(pa);

		final Value<String> l = new DublinCoreAnnotator(t).readLanguage();
		Assert.assertNotNull(l);
		Assert.assertEquals("en", l.firstTypedValue());
	}


	@Test
	public void testService3() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(true);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final Text t = WebLabResourceFactory.createResource("test", "test3", Text.class);
		t.setContent(ServiceTest.ENGLISH_SENTENCE);

		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(t);

		le.process(pa);

		final Value<String> l = new DublinCoreAnnotator(t).readLanguage();
		Assert.assertNotNull(l);
		Assert.assertEquals("en", l.firstTypedValue());
	}


	@Test
	public void testService4() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(false);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final Text t = WebLabResourceFactory.createResource("test", "test4", Text.class);
		t.setContent(ServiceTest.ENGLISH_SENTENCE);

		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(t);

		le.process(pa);

		final Value<String> l = new DublinCoreAnnotator(t).readLanguage();
		Assert.assertNotNull(l);
		Assert.assertEquals(0, l.size());
	}


	@Test(expected = InvalidParameterException.class)
	public void testBadArgService1() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(false);
		final LanguageExtraction le = new LanguageExtraction(lec);
		le.process(null);
	}


	@Test
	public void testService5() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(false);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final ProcessArgs pa = new ProcessArgs();
		final Resource res = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		pa.setResource(res);
		final ProcessReturn pr = le.process(pa);
		Assert.assertNotNull(pr);
		Assert.assertNotNull(pr.getResource());
		Assert.assertSame(res, pr.getResource());
	}


	@Test
	public void testService6() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(false);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final ProcessArgs pa = new ProcessArgs();
		final Resource res = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		pa.setResource(res);
		final ProcessReturn pr = le.process(pa);
		Assert.assertNotNull(pr);
		Assert.assertNotNull(pr.getResource());
		Assert.assertSame(res, pr.getResource());
	}


	@Test
	public void testService7() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(true);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final ProcessArgs pa = new ProcessArgs();
		final Document res = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t.setContent(ServiceTest.ENGLISH_SENTENCE);
		final Text t2 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t2.setContent(ServiceTest.ENGLISH_SENTENCE);
		WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		final Text t4 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t4.setContent("");
		final Text t5 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t5.setContent("                                 ");
		final Text t6 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t6.setContent("         toto                        ");
		pa.setResource(res);
		final ProcessReturn pr = le.process(pa);
		Assert.assertNotNull(pr);
		Assert.assertNotNull(pr.getResource());
		Assert.assertSame(res, pr.getResource());
		Assert.assertNotNull(le.toString());
	}


	@Test
	public void testService8() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(true);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final ProcessArgs pa = new ProcessArgs();
		final Document res = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		final Text t2 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t2.setContent("");
		pa.setResource(res);
		final ProcessReturn pr = le.process(pa);
		Assert.assertNotNull(pr);
		Assert.assertNotNull(pr.getResource());
		Assert.assertSame(res, pr.getResource());
		Assert.assertNotNull(le.toString());
	}


	@Test
	public void testService9() throws Exception {

		final LanguageExtractionConfiguration lec = new LanguageExtractionConfiguration();
		lec.setMaxNbValues(2);
		lec.setAddTopLevelAnnot(true);
		lec.setAddMediaUnitLevelAnnot(false);
		final LanguageExtraction le = new LanguageExtraction(lec);
		final ProcessArgs pa = new ProcessArgs();
		final Document res = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		final Text t = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t.setContent(ServiceTest.ENGLISH_SENTENCE);
		final Text t2 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t2.setContent(ServiceTest.ENGLISH_SENTENCE);
		WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		final Text t4 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t4.setContent("");
		final Text t5 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t5.setContent("                                 ");
		final Text t6 = WebLabResourceFactory.createAndLinkMediaUnit(res, Text.class);
		t6.setContent("         toto                        ");
		pa.setResource(res);
		final ProcessReturn pr = le.process(pa);
		Assert.assertNotNull(pr);
		Assert.assertNotNull(pr.getResource());
		Assert.assertSame(res, pr.getResource());
		Assert.assertNotNull(le.toString());
	}

}
