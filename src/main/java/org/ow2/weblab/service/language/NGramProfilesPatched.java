/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.language;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;

import de.spieleck.app.cngram.NGram;
import de.spieleck.app.cngram.NGramProfile;
import de.spieleck.app.cngram.NGramProfileImpl;
import de.spieleck.app.cngram.NGramProfiles;

/**
 * This is a patched version of NGramProfiles class from CNGram project.
 *
 * It has been added to enable the use of custom language models. When using the default version, it's only possible to load languages models that are present in the jar, next to the NGramProfiles
 * class. Thus this class overrides original class and has to implement its own version of the init method.
 *
 * "Manage a set of profiles and determine "most similar" ones to a given profile. Allows access to the complete results of previous last ranking. Note this uses a competitive ranking approach, which
 * is memory efficient, time efficient for not too many languages and provides contextual scoring of ngrams."
 *
 * @author Inspired from frank nestel
 * @author EADS IPCC Team
 * @see de.spieleck.app.cngram.NGramProfiles
 */
public class NGramProfilesPatched extends NGramProfiles {



	/**
	 * Creates an instance loading default models provided inside the cngram jar
	 *
	 * @param mode
	 *            The mode used inside ngramj for ranking models
	 * @throws IOException
	 *             If the default models cannot be started
	 * @throws ReflectiveOperationException
	 *             If an error occurred accessing (and changing value) of private fields of original NgramJ class
	 */
	public NGramProfilesPatched(final int mode) throws IOException, ReflectiveOperationException {

		super(null);
		this.initFromDefault(mode);
	}



	/**
	 * Creates an instance loading models from the provided folder
	 *
	 * @param folder
	 *            The folder containing the NGramProfile files
	 * @param mode
	 *            The ranking mode. Not described in original ngramj profile class but it has impact on results
	 * @throws IOException
	 *             if an error occurs accessing the folder or reading the files inside
	 * @throws ReflectiveOperationException
	 *             If an error occurred accessing (and changing value) of private fields of original NgramJ class
	 */
	public NGramProfilesPatched(final File folder, final int mode) throws IOException, ReflectiveOperationException {

		super(null);
		this.init(folder, mode);
	}


	@Override
	protected void init(final BufferedReader br) throws IOException {

		// Doing nothing to prevent from old default behaviour
	}


	/**
	 * Replace original init method to be able to access resources inside original jar.
	 *
	 * @param mode
	 *            The mode used for scoring
	 *
	 * @throws IOException
	 *             if an error occurs accessing the profiles.lst or the profiles inside the original cngram jar
	 * @throws ReflectiveOperationException
	 *             If an error occurred accessing (and changing value) of private fields of original NgramJ class
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void initFromDefault(final int mode) throws IOException, ReflectiveOperationException {

		final Field privateProfileField = NGramProfiles.class.getDeclaredField("profiles");
		privateProfileField.setAccessible(true);
		final List<Object> profilesReference = new ArrayList<>();
		privateProfileField.set(this, profilesReference);

		final Field privateFirstNGramsField = NGramProfiles.class.getDeclaredField("firstNGrams");
		privateFirstNGramsField.setAccessible(true);
		privateFirstNGramsField.setInt(this, 0);

		final Field privateModeField = NGramProfiles.class.getDeclaredField("mode");
		privateModeField.setAccessible(true);
		privateModeField.setInt(this, mode);

		final Field privateMaxLenField = NGramProfiles.class.getDeclaredField("maxLen");
		privateMaxLenField.setAccessible(true);

		final Field privateAllNGramsField = NGramProfiles.class.getDeclaredField("allNGrams");
		privateAllNGramsField.setAccessible(true);
		final Set allNGramsRef = (Set) privateAllNGramsField.get(this);

		final Field privateMyTrieField = NGramProfiles.class.getDeclaredField("myTrie");
		privateMyTrieField.setAccessible(true);

		try (final BufferedReader br = IOUtils.buffer(new InputStreamReader(NGramProfiles.class.getResourceAsStream("/profiles.lst")))) {

			String line;
			while ((line = br.readLine()) != null) {
				if (line.charAt(0) == '#') {
					continue;
				}
				try (InputStream is = NGramProfiles.class.getResourceAsStream("/" + line + "." + NGramProfile.NGRAM_PROFILE_EXTENSION);) {
					final NGramProfileImpl np = new NGramProfileImpl(line);
					np.load(is);

					profilesReference.add(np);
					for (final Iterator<?> iterator = np.getSorted(); iterator.hasNext();) {
						final NGram ng = (NGram) iterator.next();
						if (ng.length() > privateMaxLenField.getInt(this)) {
							privateMaxLenField.setInt(this, ng.length());
						}
						privateFirstNGramsField.setInt(this, privateFirstNGramsField.getInt(this) + 1);
						allNGramsRef.add(ng);
					}
				}
			}
		}
		privateMyTrieField.set(this, null);
	}


	/**
	 * @param folder
	 *            The folder containing the NGramProfile files
	 * @param mode
	 *            The mode used for scoring
	 * @throws IOException
	 *             if an error occurs accessing the folder or reading the files inside
	 * @throws ReflectiveOperationException
	 *             If an error occurred accessing (and changing value) of private fields of original NgramJ class
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void init(final File folder, final int mode) throws IOException, ReflectiveOperationException {

		final Field privateProfileField = NGramProfiles.class.getDeclaredField("profiles");
		privateProfileField.setAccessible(true);
		final List<Object> profilesReference = new ArrayList<>();
		privateProfileField.set(this, profilesReference);

		final Field privateFirstNGramsField = NGramProfiles.class.getDeclaredField("firstNGrams");
		privateFirstNGramsField.setAccessible(true);
		privateFirstNGramsField.setInt(this, 0);

		final Field privateModeField = NGramProfiles.class.getDeclaredField("mode");
		privateModeField.setAccessible(true);
		privateModeField.setInt(this, mode);

		final Field privateMaxLenField = NGramProfiles.class.getDeclaredField("maxLen");
		privateMaxLenField.setAccessible(true);

		final Field privateAllNGramsField = NGramProfiles.class.getDeclaredField("allNGrams");
		privateAllNGramsField.setAccessible(true);
		final Set allNGramsRef = (Set) privateAllNGramsField.get(this);

		final Field privateMyTrieField = NGramProfiles.class.getDeclaredField("myTrie");
		privateMyTrieField.setAccessible(true);

		final File[] files = folder.listFiles(new FilenameFilter() {


			@Override
			public boolean accept(final File dir, final String name) {

				return name.endsWith("." + NGramProfile.NGRAM_PROFILE_EXTENSION);
			}

		});
		if ((files == null) || (files.length == 0)) {
			throw new IOException("No profile found in directory " + folder.getAbsolutePath());
		}
		for (final File file : files) {
			final String name = file.getName();
			final NGramProfileImpl np = new NGramProfileImpl(name.replace("." + NGramProfile.NGRAM_PROFILE_EXTENSION, ""));
			try (final FileInputStream fis = new FileInputStream(file)) {
				np.load(fis);
			}
			profilesReference.add(np);
			for (final Iterator<?> iterator = np.getSorted(); iterator.hasNext();) {
				final NGram ng = (NGram) iterator.next();
				if (ng.length() > privateMaxLenField.getInt(this)) {
					privateMaxLenField.setInt(this, ng.length());
				}
				privateFirstNGramsField.setInt(this, privateFirstNGramsField.getInt(this) + 1);
				allNGramsRef.add(ng);
			}
		}
		privateMyTrieField.set(this, null);
	}


	@Override
	public String toString() {

		final StringBuilder sb = new StringBuilder();
		sb.append("NGramProfilesPatched has " + this.getProfileCount() + " language profiles: [");
		for (int p = 0; p < this.getProfileCount(); p++) {
			sb.append(this.getProfileName(p));
			if (p < (this.getProfileCount() - 1)) {
				sb.append(", ");
			} else {
				sb.append("]");
			}
		}
		return sb.toString();
	}

}
