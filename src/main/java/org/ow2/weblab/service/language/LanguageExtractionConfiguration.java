/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.language;

import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class is an holder for the configuration of the service
 *
 * It can be configured using one of its custom fields that enable the specification of those 11 parameters:
 * <ul>
 * <li>maxNbValues: It's a positive integer value. The list of annotated language on a given Text could not be greater that this value.</li>
 * <li>addTopLevelAnnot: It's a boolean value. It defines whether or not to annotate the whole document with the language extracted from the concatenation of every Text content.</li>
 * <li>addMediaUnitLevelAnnot: It's a boolean value. It defines whether or not to annotate the each Text section with the language guessed.</li>
 * <li>profilesFolderPath: It's a String that represents a folder path; This folder contains .ngp files that will be loaded instead of default CNGram 28 languages.</li>
 * <li>isProducedByObject: It's a String value that should be a valid URI. It defines the URI to be used as object of every isProducedBy statements on annotations created by the service.</li>
 * <li>unknownLanguageCode: It's the String value that will be annotated when no language can be clearly identified. When <code>null</code>, nothing is annotated.</li>
 * <li>minSingleValue: It's a double value between 0 and 1. If the best language score is greater than this value, it will be the only one annotated on a given Text.</li>
 * <li>minMultipleValue: It's a double value between 0 and 1. Every language score that are greater than this value, will be annotated on a given Text.</li>
 * <li>minContentLength: It's an Integer. The minimum length of the String to be analysed. Otherwise it is simply ignored.</li>
 * <li>pathToBlackList: The path to the black list file (containing string to be ignored from the ngrams)</li>
 * <li>mode: It's an integer value. This is used inside Cngram internal algorithm and has impact on the score computation.</li>
 * </ul>
 *
 * Those 11 properties are optional. Default values are:
 * <ul>
 * <li>maxNbValues: '1'</li>
 * <li>addTopLevelAnnot: <code>false</code></li>
 * <li>addMediaUnitLevelAnnot: <code>true</code></li>
 * <li>profilesFolderPath: in this case, we use the default constructor for CNGram profile that will use default profile given in their jar file. These 28 profiles are named using ISO 639-1 two
 * letters language code; it means that the DC:language annotation resulting will be in this format. If you want to use another format, you have use a custom profiles folder (containing .ngp
 * files).</li>
 * <li>isProducedByObject: <code>null</code> in this case, no isProducedBy annotation will be created.</li>
 * <li>unknownLanguageCode: 'und' is used, since it is the code for undetermined in ISO-639-x.</li>
 * <li>minSingleValue: '0.75'</li>
 * <li>minMultipleValue: '0.15'</li>
 * <li>minContentLength: '5', i.e. almost every text is processed.</li>
 * <li>pathToBlackList: 'null', i.e. nothing is ignored.</li>
 * <li>mode: '1'</li>
 * </ul>
 *
 * @author ymombrun
 *
 */
public class LanguageExtractionConfiguration {


	/**
	 * The name of the service to be used in the logs
	 */
	public static final String SERVICE_NAME = "NgramJ Language Extraction";


	private static final boolean DEFAULT_ADD_MEDIA_UNIT_LEVEL_ANNOT = true;


	private static final boolean DEFAULT_ADD_TOP_LEVEL_ANNOT = false;


	private static final URI DEFAULT_IS_PRODUCED_BY_OBJECT = null;


	private static final int DEFAULT_MAX_NB_VALUES = 1;


	private static final double DEFAULT_MIN_MULTIPLE_VALUE = 0.15;


	private static final double DEFAULT_MIN_SINGLE_VALUE = 0.75;


	private static final int DEFAULT_MIN_CONTENT_LENGTH = 5;


	private static final String DEFAULT_PROFILES_FOLDER_PATH = null;


	private static final String DEFAULT_UNKNOWN_LANGUAGE_CODE = "und";


	private static final String DEFAULT_PATH_TO_BLACKLIST = null;


	private static final int DEFAULT_MODE = 1;



	/**
	 * Whether to annotate each text section received.
	 */
	private boolean addMediaUnitLevelAnnot = LanguageExtractionConfiguration.DEFAULT_ADD_MEDIA_UNIT_LEVEL_ANNOT;


	/**
	 * Whether to annotate the document if the received resource is a document.
	 */
	private boolean addTopLevelAnnot = LanguageExtractionConfiguration.DEFAULT_ADD_TOP_LEVEL_ANNOT;


	/**
	 * The URI to be annotated with isProducedBy statement on each Annotation created. If null, no statement is added.
	 */
	private URI isProducedByObject = LanguageExtractionConfiguration.DEFAULT_IS_PRODUCED_BY_OBJECT;


	/**
	 * The maximum number of language to be annotated on a single mediaUnit.
	 */
	private int maxNbValues = LanguageExtractionConfiguration.DEFAULT_MAX_NB_VALUES;


	/**
	 * The minimum score of second, third and following recognised profiles to be annotated (if maxBnValues not reached and if the first is smaller than minSingleValue).
	 */
	private double minMultipleValue = LanguageExtractionConfiguration.DEFAULT_MIN_MULTIPLE_VALUE;


	/**
	 * The minimum score to be reached to force a single language to be identified
	 */
	private double minSingleValue = LanguageExtractionConfiguration.DEFAULT_MIN_SINGLE_VALUE;


	/**
	 * Threshold for text content length. If the input text is shorter than this, the service do not make prediction and return unknown language
	 */
	private int minContentLength = LanguageExtractionConfiguration.DEFAULT_MIN_CONTENT_LENGTH;


	/**
	 * The language code to be used when no majority is found when guessing language. If null, nothing is annotated.
	 */
	private String unknownLanguageCode = LanguageExtractionConfiguration.DEFAULT_UNKNOWN_LANGUAGE_CODE;


	/**
	 * Path to a file containing terms to blacklist (ie remove them before text analysis). Note that a specific format is expected (see sample in the project source)
	 */
	private String pathToBlackList = LanguageExtractionConfiguration.DEFAULT_PATH_TO_BLACKLIST;


	/**
	 * Path to the folder that contains the profiles to load (or null and in that case will really on default ngramj behavior i.e. load from profiles.lst file in classpath)
	 */
	private String profilesFolderPath = LanguageExtractionConfiguration.DEFAULT_PROFILES_FOLDER_PATH;


	/**
	 * The mode used inside ngramj internal algorithm to compute ranking
	 */
	private int mode = LanguageExtractionConfiguration.DEFAULT_MODE;


	/**
	 * The logger used inside the class
	 */
	private final Log log;


	/**
	 * Default constructor, just initiates the logs
	 */
	public LanguageExtractionConfiguration() {

		this.log = LogFactory.getLog(this.getClass());
	}


	/**
	 * Validate the configuration prior to use it.
	 * 
	 * @throws IllegalArgumentException
	 *             If the provided configuration is not valid...
	 */
	public void validateConfig() throws IllegalArgumentException {

		if (this.maxNbValues < 1) {
			final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">'" + " Error initialising service. Parameter maxNbValues (" + this.maxNbValues
					+ ") must be greater than 0 to enable annotation.";
			this.log.error(msg);
			throw new IllegalArgumentException(msg);
		}

		if ((this.minSingleValue < 0) || (this.minSingleValue > 1)) {
			final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">'" + " Error initialising service. Parameter minSingleValue (" + this.minSingleValue
					+ ") must be between 0 and 1.";
			this.log.error(msg);
			throw new IllegalArgumentException(msg);
		}
		if ((this.minMultipleValue < 0) || (this.minMultipleValue > 1)) {
			final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">'" + " Error initialising service. Parameter minMultipleValue (" + this.minMultipleValue
					+ ") must be between 0 and 1.";
			this.log.error(msg);
			throw new IllegalArgumentException(msg);
		}
		if (this.minSingleValue < this.minMultipleValue) {
			final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">'" + " Error initialising service. Parameter minSingleValue (" + this.minSingleValue
					+ ") must be bigger than minMultipleValue (" + this.minMultipleValue + ").";
			this.log.error(msg);
			throw new IllegalArgumentException(msg);
		}

	}


	/**
	 * @return the addMediaUnitLevelAnnot
	 */
	public boolean isAddMediaUnitLevelAnnot() {

		return this.addMediaUnitLevelAnnot;
	}


	/**
	 * @param addMediaUnitLevelAnnot
	 *            the addMediaUnitLevelAnnot to set
	 */
	public void setAddMediaUnitLevelAnnot(final boolean addMediaUnitLevelAnnot) {

		this.addMediaUnitLevelAnnot = addMediaUnitLevelAnnot;
	}


	/**
	 * @return the addTopLevelAnnot
	 */
	public boolean isAddTopLevelAnnot() {

		return this.addTopLevelAnnot;
	}


	/**
	 * @param addTopLevelAnnot
	 *            the addTopLevelAnnot to set
	 */
	public void setAddTopLevelAnnot(final boolean addTopLevelAnnot) {

		this.addTopLevelAnnot = addTopLevelAnnot;
	}


	/**
	 * @return the isProducedByObject
	 */
	public URI getIsProducedByObject() {

		return this.isProducedByObject;
	}


	/**
	 * @param isProducedByObject
	 *            the isProducedByObject to set
	 */
	public void setIsProducedByObject(final String isProducedByObject) {

		if (isProducedByObject == null) {
			this.isProducedByObject = null;
		} else {
			this.isProducedByObject = URI.create(isProducedByObject);
		}

	}




	/**
	 * @return the maxNbValues
	 */
	public int getMaxNbValues() {

		return this.maxNbValues;
	}




	/**
	 * @param maxNbValues
	 *            the maxNbValues to set
	 */
	public void setMaxNbValues(final int maxNbValues) {

		this.maxNbValues = maxNbValues;
	}




	/**
	 * @return the minMultipleValue
	 */
	public double getMinMultipleValue() {

		return this.minMultipleValue;
	}




	/**
	 * @param minMultipleValue
	 *            the minMultipleValue to set
	 */
	public void setMinMultipleValue(final double minMultipleValue) {

		this.minMultipleValue = minMultipleValue;
	}




	/**
	 * @return the minSingleValue
	 */
	public double getMinSingleValue() {

		return this.minSingleValue;
	}




	/**
	 * @param minSingleValue
	 *            the minSingleValue to set
	 */
	public void setMinSingleValue(final double minSingleValue) {

		this.minSingleValue = minSingleValue;
	}




	/**
	 * @return the minContentLength
	 */
	public int getMinContentLength() {

		return this.minContentLength;
	}




	/**
	 * @param minContentLength
	 *            the minContentLength to set
	 */
	public void setMinContentLength(final int minContentLength) {

		this.minContentLength = minContentLength;
	}




	/**
	 * @return the unknownLanguageCode
	 */
	public String getUnknownLanguageCode() {

		return this.unknownLanguageCode;
	}




	/**
	 * @param unknownLanguageCode
	 *            the unknownLanguageCode to set
	 */
	public void setUnknownLanguageCode(final String unknownLanguageCode) {

		this.unknownLanguageCode = unknownLanguageCode;
	}




	/**
	 * @return the pathToBlackList
	 */
	public String getPathToBlackList() {

		return this.pathToBlackList;
	}




	/**
	 * @param pathToBlackList
	 *            the pathToBlackList to set
	 */
	public void setPathToBlackList(final String pathToBlackList) {

		this.pathToBlackList = pathToBlackList;
	}




	/**
	 * @return the profilesFolderPath
	 */
	public String getProfilesFolderPath() {

		return this.profilesFolderPath;
	}




	/**
	 * @param profilesFolderPath
	 *            the profilesFolderPath to set
	 */
	public void setProfilesFolderPath(final String profilesFolderPath) {

		this.profilesFolderPath = profilesFolderPath;
	}




	/**
	 * @return the mode
	 */
	public int getMode() {

		return this.mode;
	}




	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(final int mode) {

		this.mode = mode;
	}


}
