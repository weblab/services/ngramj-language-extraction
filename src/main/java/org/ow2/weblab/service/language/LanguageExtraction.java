/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.language;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.purl.dc.terms.DCTermsAnnotator;

import de.spieleck.app.cngram.NGramProfiles.RankResult;
import de.spieleck.app.cngram.NGramProfiles.Ranker;

/**
 * This class is a WebLab Web service for identifying the language of a Text.
 *
 *
 * It's a wrapper of the NGramJ project: "http://ngramj.sourceforge.net/". It uses the CNGram system that can computes character string instead of raw text files.
 *
 *
 * This algorithm return for each input text a score associated to every language profile previously learned (.ngp files). The score is a double between '0' and '1'. '1' meaning that this text is
 * written in this language for sure. '0' on the opposite means that this text is not written in this language. The sum of score equals '1'.
 *
 *
 * Our wrapper annotate every Text section of a Document in input (or the Text if the input is a Text). It fails if the input is something else. On each Text it uses CNGram to determine which language
 * profile are the best candidate to be annotated (using DC:language property).
 *
 * @author EADS IPCC Team
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class LanguageExtraction implements Analyser {



	/**
	 * The logger used inside the class
	 */
	private final Log log;


	/**
	 * The profiles used to guess language. These profiles are loaded from a folder or from the classpath.
	 */
	private final NGramProfilesPatched ngps;


	/**
	 * Regex pattern compiling all the terms to blacklist
	 */
	private final Pattern blPattern;


	/**
	 * The configuration holder
	 */
	private final LanguageExtractionConfiguration conf;


	/**
	 * The default constructor that initialises an instance using the default {@link LanguageExtractionConfiguration}.
	 */
	public LanguageExtraction() {

		this(new LanguageExtractionConfiguration());
	}


	/**
	 * @param conf
	 *            Instance of configuration object used to customise various parameter as described in {@link LanguageExtractionConfiguration};
	 */
	public LanguageExtraction(final LanguageExtractionConfiguration conf) {

		this.log = LogFactory.getLog(this.getClass());
		conf.validateConfig(); // Throws RuntimeException if not valid
		this.conf = conf;

		this.ngps = this.loadProfiles();

		if (this.conf.getPathToBlackList() == null) {
			this.blPattern = null;
		} else {
			this.blPattern = this.blackListWithFreqCompilation(this.conf.getPathToBlackList());
		}

		this.log.info("'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">'" + " successfully initialised with configuration: " + this.toString());
	}


	/**
	 * Load terms to blacklist from a specific file that also give the frequencies of each terms in order to optimise the filtering process. See sample from the project source.
	 *
	 * @param pathToBlackList_p
	 *            The path to the blacklist file to be used.
	 * @return The pattern to be applied on each string and removing the black list characters.
	 * @throws IllegalArgumentException
	 *             If the file at a non null path is not a readable existing file
	 */
	protected Pattern blackListWithFreqCompilation(final String pathToBlackList_p) throws IllegalArgumentException {

		final char[] regexMetaChar = { '<', '(', '[', '{', '\\', '^', '-', '=', '$', '!', '|', ']', '}', ')', '?', '*', '+', '.', '>' };

		final Set<Character> regexMetaCharSet = new HashSet<>(regexMetaChar.length);
		for (final char c : regexMetaChar) {
			regexMetaCharSet.add(Character.valueOf(c));
		}

		final Set<String> blackTermWithFreq = new HashSet<>();

		final File pth = new File(pathToBlackList_p);
		if (!pth.exists()) {
			final String message = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. Provided path to black list file '" + pathToBlackList_p
					+ "' is resolved to a non existing file '" + pth.getAbsolutePath() + "'.";
			this.log.error(message);
			throw new IllegalArgumentException(message);
		}
		if (!pth.isFile()) {
			final String message = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. Provided path to black list file '" + pathToBlackList_p
					+ "' is resolved to a non regular file '" + pth.getAbsolutePath() + "'.";
			this.log.error(message);
			throw new IllegalArgumentException(message);
		}
		if (!pth.canRead()) {
			final String message = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. Provided path to black list file '" + pathToBlackList_p
					+ "' is resolved to a non readable file '" + pth.getAbsolutePath() + "'.";
			this.log.error(message);
			throw new IllegalArgumentException(message);
		}
		try (final LineNumberReader rdr = new LineNumberReader(new FileReader(pth))) {
			String line = null;
			while ((line = rdr.readLine()) != null) {
				blackTermWithFreq.add(line);
			}
		} catch (final IOException ioe) {
			final String message = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. An error occured reading provided path to black list file '"
					+ pathToBlackList_p + "' which is resolved to '" + pth.getAbsolutePath() + "'.";
			this.log.error(message, ioe);
			throw new IllegalArgumentException(message, ioe);
		}

		final TreeMap<Integer, String> weightedTerms = new TreeMap<>();
		for (final String term : blackTermWithFreq) {
			final String[] values = term.split("\t");
			int weight = Integer.parseInt(values[0]);
			final String blackTerm = values[1];

			// decrease weight if already present, ugly but safe
			while (weightedTerms.containsKey(Integer.valueOf(weight))) {
				weight--;
			}

			weightedTerms.put(Integer.valueOf(weight), blackTerm);
		}

		Entry<Integer, String> wbt = null;

		final StringBuffer bigRegex = new StringBuffer();
		bigRegex.append('(');
		while ((wbt = weightedTerms.pollLastEntry()) != null) {
			final String term = wbt.getValue();
			// TODO try better optimisation based on fusing terms with common starting characters
			final StringBuffer regex = new StringBuffer();
			for (final char i : term.toCharArray()) {
				if (regexMetaCharSet.contains(Character.valueOf(i))) {
					regex.append("\\");
				}
				regex.append(i);
			}
			final Pattern p = Pattern.compile(regex.toString());
			if (p.matcher(term).matches()) {
				if (bigRegex.length() > 2) {
					bigRegex.append('|');
				}
				bigRegex.append(regex.toString());
			} else {
				this.log.warn(
						"'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. Unable to compile pattern to detect [" + term + "]. It will not be in the blacklist.");
			}
		}
		bigRegex.append(')');

		this.log.trace("Filtering pattern from blacklist:\n" + bigRegex.toString());

		return Pattern.compile(bigRegex.toString());
	}


	/**
	 * Loads a NGramProfilesPatched from the parameter or uses default one (if null or empty).
	 *
	 * @return An instance of NGramProfilesPatched
	 */
	public NGramProfilesPatched loadProfiles() {

		final NGramProfilesPatched profiles;
		if ((this.conf.getProfilesFolderPath() == null) || this.conf.getProfilesFolderPath().isEmpty()) {
			this.log.trace("Loading default NGramProfile.");
			try {
				profiles = new NGramProfilesPatched(this.conf.getMode());
			} catch (final Exception e) {
				final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. Unable to load the default NGramProfilesPatched.";
				this.log.error(msg, e);
				throw new IllegalArgumentException(msg, e);
			}
		} else {
			final File file = new File(this.conf.getProfilesFolderPath());
			final String defaultErrorMsg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME
					+ ">': error initialising service. Unable to load NGramProfilesPatched using parameter profilesFolderPath (" + this.conf.getProfilesFolderPath() + "). The file '"
					+ file.getAbsolutePath() + "'";
			if (!file.exists()) {
				final String msg = defaultErrorMsg + " does not exists.";
				this.log.error(msg);
				throw new IllegalArgumentException(msg);
			}
			if (!file.canRead()) {
				final String msg = defaultErrorMsg + " is not readable.";
				this.log.error(msg);
				throw new IllegalArgumentException(msg);
			}
			if (!file.isDirectory()) {
				final String msg = defaultErrorMsg + " is not a directory.";
				this.log.error(msg);
				throw new IllegalArgumentException(msg);
			}
			try {
				profiles = new NGramProfilesPatched(file, this.conf.getMode());
			} catch (final Exception e) {
				final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error initialising service. Unable to load the NGramProfilesPatched using parameter profilesFolderPath ("
						+ this.conf.getProfilesFolderPath() + " resolved to " + file.getAbsolutePath() + ".";
				this.log.error(msg, e);
				throw new IllegalArgumentException(msg, e);
			}
		}
		return profiles;
	}


	@Override
	public ProcessReturn process(final ProcessArgs processArgs) throws InvalidParameterException {

		this.log.trace("Process method called.");

		final List<Text> texts = this.checkArgs(processArgs);
		final Resource resource = processArgs.getResource();

		final String source = new DublinCoreAnnotator(resource).readSource().firstTypedValue();
		final String resourceUri = resource.getUri();
		this.log.debug("'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': start processing document '<" + resourceUri + ">'" + " - " + "'<" + source + ">'");

		final boolean topLevelAnnot = this.conf.isAddTopLevelAnnot() && !texts.isEmpty() && !(resource instanceof Text);

		final StringBuilder sb = new StringBuilder();
		for (final Text text : texts) {
			if ((text.getContent() == null) || text.getContent().isEmpty()) {
				this.log.trace("Text '" + text.getUri() + "' has no content. Ignoring it.");
				continue;
			}
			if (this.conf.isAddMediaUnitLevelAnnot()) {
				final List<String> profileToAnnotate = this.checkLanguage(text.getContent(), text.getUri());
				this.annotate(text, profileToAnnotate);
			}
			if (topLevelAnnot) {
				sb.append(text.getContent());
				sb.append("\n\n");
			}
		}

		if (topLevelAnnot && (sb.length() > 0)) {
			final List<String> profileToAnnotate = this.checkLanguage(sb.toString(), resourceUri);
			this.annotate(resource, profileToAnnotate);
		}

		this.log.debug("'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': end processing document '<" + resourceUri + ">'" + " - " + "'<" + source + ">'");
		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(resource);
		return pr;
	}


	@Override
	public String toString() {

		final StringBuilder builder = new StringBuilder();
		builder.append("LanguageExtraction [conf=");
		builder.append(this.conf);
		builder.append(", blPattern=");
		builder.append(this.blPattern);
		builder.append("]");
		return builder.toString();
	}


	/**
	 * @param res
	 *            The resource to be annotated
	 * @param profileToAnnotate
	 *            The language to annotate using dc:language property statements on res.
	 */
	private void annotate(final Resource res, final List<String> profileToAnnotate) {

		if (profileToAnnotate.isEmpty()) {
			this.log.trace("No language to annotate on resource " + res.getUri() + ".");
		} else {
			this.log.trace("Annotate Resource " + res.getUri() + " with following languages " + profileToAnnotate + ".");

			final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(res);
			final DublinCoreAnnotator dca = new DublinCoreAnnotator(URI.create(res.getUri()), annot);
			for (final String language : profileToAnnotate) {
				dca.writeLanguage(language);
			}
			if (this.conf.getIsProducedByObject() != null) {
				new DCTermsAnnotator(URI.create(annot.getUri()), annot).writeCreated(Calendar.getInstance().getTime());
				new WProcessingAnnotator(URI.create(annot.getUri()), annot).writeProducedBy(this.conf.getIsProducedByObject());
			}
		}
	}


	/**
	 * @param processArg
	 *            The processArgs; i.e. a usageContext not used and a Resource that should contain at least one text.
	 * @return A list of text contained in the resource in processArgs
	 * @throws InvalidParameterException
	 *             If processArgs is null, or if the resource is null.
	 */
	private List<Text> checkArgs(final ProcessArgs processArg) throws InvalidParameterException {

		if (processArg == null) {
			final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error processing document '<" + null + ">'" + " - " + "'<" + null + ">' ProcessArgs was null.";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		final Resource res = processArg.getResource();
		if (res == null) {
			final String msg = "'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': error processing document '<" + null + ">'" + " - " + "'<" + null + ">' Resource in ProcessArgs was null.";
			this.log.error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}

		final List<Text> texts;
		if (res instanceof Text) {
			texts = Collections.singletonList((Text) res);
		} else {
			texts = ResourceUtil.getSelectedSubResources(res, Text.class);
		}

		if (texts.isEmpty()) {
			this.log.warn("'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': unable to properly process document '<" + res.getUri() + ">'" + " - " + "'<" + null
					+ ">' No Text unit found in resource. Nothing will be done.");
		}

		return texts;
	}


	/**
	 * @param content
	 *            The text to identify language
	 * @param uri
	 *            The uri, used for logging purpose.
	 * @return An ordered list of language identified according to parameters (minSingleValue, maxNbValues and minMultipleValue).
	 */
	private synchronized List<String> checkLanguage(final String content, final String uri) {

		final StringBuffer contentBuff = new StringBuffer();

		// reject text content which are too short
		if (content.length() <= this.conf.getMinContentLength()) {
			this.log.trace("Text content length is under threshold, return \"und\" language.");
			final List<String> und = new ArrayList<>(1);
			und.add(this.conf.getUnknownLanguageCode());
			return und;
		}

		// remove terms from black list to avoid confusion
		if (this.blPattern == null) {
			contentBuff.append(content);
		} else {
			synchronized (this.blPattern) {
				final Matcher m = this.blPattern.matcher(content);
				if (m.find()) {
					this.log.trace("Term matched in blacklist: " + m.group(0));
					contentBuff.append(m.replaceAll(" "));
				} else {
					contentBuff.append(content);
				}
			}
		}

		final Ranker ranker = this.ngps.getRanker();
		ranker.account(contentBuff.toString());
		final RankResult result = ranker.getRankResult();

		this.log.trace("Language detected for MediaUnit '" + uri + "' are " + result.toString());

		final List<String> profileToAnnotate;
		// Profiles are listed in their rank order
		final double bestScore = result.getScore(0);
		if (bestScore > this.conf.getMinSingleValue()) {
			profileToAnnotate = Collections.singletonList(result.getName(0));
		} else if (bestScore < this.conf.getMinMultipleValue()) {
			this.log.warn("'<" + LanguageExtractionConfiguration.SERVICE_NAME + ">': unable to properly process document '<" + uri + ">'" + " - " + "'<" + null + ">'. Unable to guess language ("
					+ result.toString() + ").");
			if (this.conf.getUnknownLanguageCode() == null) {
				profileToAnnotate = Collections.emptyList();
			} else {
				profileToAnnotate = Collections.singletonList(this.conf.getUnknownLanguageCode());
			}
		} else {
			final int max = Math.min(result.getLength(), this.conf.getMaxNbValues());
			profileToAnnotate = new LinkedList<>();
			for (int p = 0; p < max; p++) {
				if (result.getScore(p) >= this.conf.getMinMultipleValue()) {
					profileToAnnotate.add(result.getName(p));
				} else {
					break;
				}
			}
		}

		return profileToAnnotate;
	}

}
